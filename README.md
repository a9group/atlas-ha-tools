# AtlasHA Tools

This repository contains the utility scripts used in the HA-enabled Atlas Playground development system.

* hot/root/ha_adddrbdvol.sh

* hot/root/ha_checkdrbdstatus.sh

* hot/root/ha_clonehot.sh

* hot/root/ha_genhaauthkeys.sh

* hot/root/ha_init.sh

* hot/root/ha_sanitycheck.sh

* hot/root/ha_switchrole.sh

* hot/root/ha_syncall.sh

* hot/root/ha_synchaconfig.sh

* util/ha_hatest.sh

## Getting Started

These tools are deployed inside the AtlasPlayground environment. They have evolved from similar tools written to manage DRBD volumes and failover for another HA-enabled project.

They are written and tested for openSUSE, but have also been deployed under Debian and Ubuntu. RHEL, Amazon Linux and CentOS should be reasonably compatible, YMMV.

### Prerequisities

* DRBD
* Multiple nodes; hot, warm, utility (See the documentation for [AtlasPlayground](http://atlassian.a9group.net)

### Installing

This repository can be cloned or unpacked from a zipfile of this repository.

## Running the tests

A test application is available to test failover through a number of
processes; 'ha_hatest.sh'
  
Run ha_hatest.sh from a Utility node to perform a suite of tests to verify HA
operability.

The script performs three tests on each host: a forced reboot, a forced failover, and forced failback.
&nbsp;_place_holder;I have run it in a few different permutations, and you can
see several functions that can be called as tests; force_reboot,
force_failover, stop_heartbeat, force_disconnect, force_standby, etc.

The execution mechanism runs through 30 calls to one host (beginning with
hot), performing one of the tests at a specified interval, currently set as
10, 20, and 25 seconds. &nbsp;_place_holder;Every 30 seconds, three pieces of
data are written to a file in /mnt on util, which is the data volume.
&nbsp;_place_holder;A timestamp, a marker, and the name of the test being
executed (if any). This logfile is written as 'ha-
test.out.YYYYmmdd'.


## Deployment

The tools are installed in the AtlasPlayground image by default.
Deployment outside of this environment requires some set up and preparation.
The tools assume LVM is used to provide a volume for DRBD for setup with ha_adddrbdvol.sh.

Use the ha_init.sh script to initialize the HA configuration.

Follow with ha_syncall.sh when hot and warm (standby) nodes are online.

## Authors

* **William Kennedy** - *Developer* - [A9 Consulting Group](https://bitbucket.org/a9group)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

